//
//  ViewController.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import UIKit
import RxSwift
import RxCocoa

class MenuViewController: UIViewController, UITableViewDelegate {
    
    // view controller activity is split in 3 sections:
    // setupUI initializes and customizes view components
    // setupViewBindings enables reactive programming
    // setupSubscription starts any subscription needed, in this case to API calls
    
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var listOrderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var offlineView: UIView!
    let disposeBag = DisposeBag()
    let viewModel = MenuViewModel()
    let networkLayer = NetworkLayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        
        setupUI()
        
        if ConnectionCheckerHelper.isInternetAvailable() {
            setUpViewBindings()
            setUpSubscription()
        }
    }
    
    func setupUI() {
        
        tableView.isHidden = true
        offlineView.isHidden = true
        offlineView.alpha = 0
        
        
        if !ConnectionCheckerHelper.isInternetAvailable() {
            UIView.animate(withDuration: 2, delay: 1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                self.offlineView.isHidden = false
                self.offlineView.alpha = 1
            })
        } else {
            tableView.isHidden = false
        }
        
        tableView.delegate = self
        tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "mainTableCell")
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    func setUpViewBindings() {
        
        // search field
        searchTextField.rx.text.orEmpty
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] text in
                self.viewModel.performSearch(for: text)
            }).disposed(by: disposeBag)
        
        // segmented control
        listOrderSegmentedControl.rx.selectedSegmentIndex
            .subscribe (onNext: { [unowned self] index in
                let orderType: ListOrder = index == 0 ? .byId : .alpha
                self.viewModel.reorderList(orderType)
            }).disposed(by: disposeBag)
        
        
        // item count
        viewModel.dataSource.asObservable().subscribe(onNext: { list in
            DispatchQueue.main.async {
                self.itemCountLabel.text = "\(list.count) current items"
            }
        }).disposed(by: disposeBag)
            
        
        
        
        // table view
        viewModel.dataSource.bind(to: self.tableView.rx.items) { (tableView, row, element) in
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainTableCell") as! MainTableViewCell
            
            cell.contentView.backgroundColor = .systemYellow
            cell.contentView.layer.cornerRadius = 20
            
            cell.pokemonNameLabel?.text = "\(element.name.capitalized) :: id \(element.pokemonID)"
            let thumbnailImageUrl = AppConfig.THUMBNAIL_IMAGE_URL.replacingOccurrences(of: "%s", with: element.pokemonID)
            
            self.networkLayer.imageFromRemote2(thumbnailImageUrl, placeHolder: UIImage(named: "placeholder")).asObservable().subscribe({
                image in
                
                guard let image = image.element else {
                    return
                }
                
                DispatchQueue.main.async {
                    cell.pokemonImageView.image = image
                }
                
            }).disposed(by: self.disposeBag)
            
            return cell
        }.disposed(by: disposeBag)
        
        // deselecting rows
        tableView.rx.itemSelected
            .subscribe(onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: true)
            }).disposed(by: disposeBag)
        
        
        tableView.rx.modelSelected(PokemonBasicData.self)
            .subscribe(onNext: { [unowned self] item in
                print("Selected item: \(item.name) \(item.pokemonID)")
                DispatchQueue.main.async {
                    self.viewModel.setSelectedPokemon(item)
                    self.performSegue(withIdentifier: "gotoPokemonDetail", sender: self)
                }
            }).disposed(by: disposeBag)
        
    }
    
    func setUpSubscription() {
        loadMoreData()
    }
    
    public func loadMoreData() {
        if ConnectionCheckerHelper.isInternetAvailable() {
            listOrderSegmentedControl.selectedSegmentIndex = UISegmentedControl.noSegment
            self.viewModel.fetchData()
        } else {
            let alert = UIAlertController(title: "Connection failure", message: "Please check your connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoPokemonDetail" {
            if let detailVC = segue.destination as? DetailViewController {
                if viewModel.selectedPokemon != nil {
                    detailVC.viewModel = DetailViewModel(selectedPokemon: viewModel.selectedPokemon!)
                }
            }
        }
    }
    
    
    //MARK: TableViewDelegate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // freezes infinite scrolling when searching
        if self.viewModel.getDataSourceCount() < AppConfig.ITEMS_PER_PAGE {
            return
        }
        
        if indexPath.row + 1 == self.viewModel.getDataSourceCount() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.loadMoreData()
            }
        }
    }
    
    
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        viewModel.setRandomPokemon()
        self.performSegue(withIdentifier: "gotoPokemonDetail", sender: self)
    }
    
    
}

