//
//  DetailViewController.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import UIKit
import RxSwift
import RxCocoa

class DetailViewController: UIViewController {
    
    // the same 3 sections approach is used here:
    // setupUI, setupViewBindings and setupSubscription
    // with the same underlying logic
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var pokemonDetailTextView: UITextView!
    var networkLayer = NetworkLayer()
    var viewModel: DetailViewModel!
    let disposeBag = DisposeBag()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupViewBindings()
        setupSubscription()
        
    }
    
    func setupUI() {
        pokemonNameLabel.adjustsFontSizeToFitWidth = true
        pokemonNameLabel.minimumScaleFactor = 0.5
        pokemonNameLabel.text = viewModel.selectedPokemon.name.uppercased()
        
        pokemonImage.makeRounded()
        pokemonImage.contentMode = .scaleAspectFit
        pokemonImage.alpha = 0

        
        pokemonDetailTextView.textAlignment = .center
        pokemonDetailTextView.text = "Loading data for \(viewModel.getSelectedPokemonName().capitalized)..."
        
        // adjusting image size for ipad
        // modifying the multiplier for the width of the pokemon image
        // which has a fixed ratio of 1:1
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            let widthConstraint = NSLayoutConstraint(item: pokemonImage!, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0.2, constant: 0)
            view.addConstraint(widthConstraint)
        } else {
            let widthConstraint = NSLayoutConstraint(item: pokemonImage!, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0.6, constant: 0)
            view.addConstraint(widthConstraint)
        }
        
    }
    
    func setupSubscription() {
        self.viewModel.fetchDetailData()
    }
    
    func setupViewBindings() {
        
        
        _ = viewModel.pokemonDetailData.asObservable().subscribe(onNext: { detail in
            
            guard let imgURL = detail.sprites?.other?.officialArtwork.frontDefault else {
                return
            }
            
            // main image observer
            
            self.networkLayer.imageFromRemote2(imgURL, placeHolder: UIImage(named: "placeholder")).asObservable().subscribe({ image in
                guard let image = image.element else {
                    return
                }
                
                UIView.animate(withDuration: 2, delay: 1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                    DispatchQueue.main.async {
                        self.pokemonImage.image = image
                        self.pokemonImage.alpha = 1
                    }
                    
                }, completion: { _ in
                    
                    // creating the attributed string for pokemon data
                    // info is taken from the API call result structure
                    // using a functional programming approach
                    // (i.e.: no for-loops or iterations)
                    
                    let stats = detail.stats?.reduce("") {
                        "\($0 ?? "") \($1.stat.name.uppercased()): \($1.baseStat) (base) \($1.effort) (effort)\n"
                    }
                    
                    let types = detail.types?.reduce("") {
                        "\($0 ?? "") \($1.type.name.uppercased())\n"
                    }
                    
                    let height = "\(detail.height ?? 0)\n"
                    let weight = "\(detail.weight ?? 0)\n"
                    
                    let attributedString = NSMutableAttributedString(string: "")
                    
                    let paragraph = NSMutableParagraphStyle()
                    paragraph.alignment = .center
                    
                    let boldAttribute: [NSAttributedString.Key: Any] =
                        [.font: UIFont.boldSystemFont(ofSize: 23), .paragraphStyle: paragraph]
                    
                    let regularAttribute: [NSAttributedString.Key: Any] =
                        [.font: UIFont.systemFont(ofSize: 21), .paragraphStyle: paragraph]
                    
                    attributedString.append(NSMutableAttributedString(string: "Stats:\n", attributes: boldAttribute))
                    attributedString.append(NSAttributedString(string: "\(stats ?? "")\n", attributes: regularAttribute))
                    attributedString.append(NSMutableAttributedString(string: "Types:\n", attributes: boldAttribute))
                    attributedString.append(NSAttributedString(string: "\(types ?? "")\n", attributes: regularAttribute))
                    
                    if height != "0" {
                        attributedString.append(NSMutableAttributedString(string: "Height:\n", attributes: boldAttribute))
                        attributedString.append(NSAttributedString(string: "\(height)\n", attributes: regularAttribute))
                    }
                    
                    if weight != "0" {
                        attributedString.append(NSMutableAttributedString(string: "Weight:\n", attributes: boldAttribute))
                        attributedString.append(NSAttributedString(string: "\(weight )\n", attributes: regularAttribute))
                    }
                    
                    self.pokemonDetailTextView.attributedText = attributedString 
                })
                
            }).disposed(by: self.disposeBag)
            
            
        },
        onError: { error in
            
            var errMsg: String
            
            switch error {
            case PokemonErrors.JSONParsingError:
                errMsg = "Sorry, could not parse detail data for \(self.viewModel.getSelectedPokemonName().capitalized)"
            default:
                errMsg = "A generic error occurred"
            }
            
            DispatchQueue.main.async {
                self.pokemonDetailTextView.text = errMsg
            }
            
        })
        .disposed(by: disposeBag)
        
    }
    
    
    @IBAction func onBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    

}
