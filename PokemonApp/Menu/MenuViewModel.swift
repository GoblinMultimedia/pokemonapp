//
//  MenuViewModel.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import Foundation
import RxSwift
import RxCocoa

enum ListOrder {
    case byId
    case alpha
}

class MenuViewModel {
    
    var urlString = AppConfig.POKEMON_BASIC_DATA_INITIAL_URL
    let dataSource: BehaviorRelay<BaseResults> = BehaviorRelay(value: [])
    var error : Error?
    let disposeBag = DisposeBag()
    var isUpdating = false
    var networkLayer = NetworkLayer()
    var selectedPokemon: PokemonBasicData?
    var fullListBeforeSearch: BaseResults = [] // to restore original list after searching
    
    
    public func fetchData() {
        
        if isUpdating { return }
        
        isUpdating = true
        
        let disposable = networkLayer.fetchDataFromAPIv2(withUrlString: urlString, modelType: PokemonBaseInfo.self)
            
            .subscribe(onNext: { [unowned self] data in
                
                guard let model = data else {
                    return
                }
                
                guard let nextURL = model.next, nextURL != self.urlString else {
                    print ("no url update")
                    return
                }
                self.urlString = nextURL
                var currentDataSource = self.dataSource.value
                currentDataSource.append(contentsOf: model.results)
                self.dataSource.accept(currentDataSource)
                // store current list state for restore on empty search string
                self.fullListBeforeSearch = self.dataSource.value
                self.isUpdating = false
                
            }, onError: { (error) in
                self.error = error
            }, onCompleted: {
                
            }) {
                print("Disposed")
            }
        disposable.disposed(by: disposeBag)
    }
    
   
    
    func reorderList(_ orderType: ListOrder) {
        
        var reorderedList: [PokemonBasicData] = []
        
        if orderType == .byId {
            reorderedList = self.dataSource.value.sorted(by: { Int($0.pokemonID) ?? 0 < Int($1.pokemonID) ?? 0 })
        } else {
            reorderedList = self.dataSource.value.sorted(by: { $0.name < $1.name })
        }
        
        self.dataSource.accept(reorderedList)
    }
    
    
    func performSearch(for searchTerm: String) {
        
        if searchTerm.isEmpty {
            dataSource.accept(fullListBeforeSearch)
        } else {
            dataSource.accept(fullListBeforeSearch.filter({ $0.name.lowercased().starts(with: searchTerm.lowercased()) }))
        }
         
    }
    
    
    
    func getDataSourceCount() -> Int {
        dataSource.value.count
    }
    
    func setSelectedPokemon(_ data: PokemonBasicData) {
        selectedPokemon = data
    }
    
    func setRandomPokemon() {
        selectedPokemon = dataSource.value.randomElement()
    }
    
}
