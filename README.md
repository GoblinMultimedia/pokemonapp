POKEMON APP
Implementazione di Paolo Piccini
paolopiccini.dev@gmail.com
3471229621

---

La restrizione imposta dal requisito di utilizzare iOS11 ha imposto delle limitazioni al progetto, che se da un lato hanno complicato lo sviluppo, dall'altro hanno reso la challenge più interessante, perché ovviamente la barriera imposta da iOS11 non ha consentito di usare né SwiftUI, né Combine, né una serie di componenti iOS come ad esempio NWPathMonitor, non disponibili per il target selezionato.

Il progetto è stato impostato, come da specifica e come da mia common practice, seguendo il pattern architetturale MVVM, che ritengo una soluzione estremamente efficiente per risolvere un problema comune a molte app iOS, ovvero quello del "Massive" View Controller, che spesso si verifica quando si usa il classico pattern MVC.

Non che non esistano sistemi per ottenere dei ViewController più "lean" anche in MVC, chiaro (penso ad esempio all'utilizzo del pattern Coordinator, che solleva il Controller dalla responsabilità della navigazione fra le view e ne permette una gestione molto più flessibile, oppure rimuovendo tutto il boilerplate code relativo alla gestione delle tableViews e gestendolo in una extension...), ma indubbiamente MVVM si presta meglio allo scopo, e soprattutto risulta più adatto alla gestione della programmazione reattiva.

Nella mia implementazione infatti ho scelto questa soluzione, utilizzando l'unica libreria che ho utilizzato nel progetto, ovvero RxSwift, che ha fatto le veci di Combine mantenendo immutato l'approccio a livello logico.

Nel loro lifecycle i ViewController che ho utilizzato nel progetto seguono uno schema suddiviso in 3 sezioni principali, ovvero:

- impostazione e customizzazione degli elementi della UI (func setupUI)
- impostazione dei bindings tramite RxSwift (func setupViewBindings)
- impostazione delle subscriptions necessarie (func setupSubscriptions)

Ad ogni ViewController è associato un ViewModel che osserva le modifiche del Model e gli input della View gestendoli di conseguenza.

Il Model è rappresentato da due Codable struct che mappano i risultati delle chiamate API.
Nel model PokemonBasicData, che rappresenta i risultati della prima chiamata API che gestisce la lista dei Pokemon, è presente una computed property (pokemonID), che mi ha consentito di utilizzare un piccolo "trick" per ottenere l'immagine thumbnail di un Pokemon specifico. 

La prima chiamata infatti restituisce solamente il nome e l'URL del dettaglio del Pokemon, e nell'url restituito sono presenti anche le URL delle immagini. Invece di prendere la prima lista restituita e di effettuare una chiamata per ogni elemento e ottenere l'immagine da quella chiamata, con un evidente aumento della complessità della gestione, delle risorse utilizzate dall'app e dei tempi di attesa, ho verificato che l'url dell'immagine era deducibile utilizzando un url fisso da raw.github appendendo all'url l'id del Pokemon, che ricavo appunto dalla computed property indicata sopra.

Ho creato un NetworkLayer che si occupa di gestire le funzioni relative alle richieste di rete; la prima funzione, fetchDataFromAPIv2, utilizza i Generics in modo da poter essere utilizzata con qualunque modelType senza duplicare il codice. Il risultato della chiamata è gestito come Observable di RxSwift e va a modificare un BehaviorRelay nel ViewModel a cui successivamente la View reagisce aggiornando la UI.

La seconda funzione del NetworkLayer gestisce in modo asincrono il recupero di immagini dalla rete, con gestione di un eventuale placeholder. Anche qui il risultato è gestito come Observable, il che mi consente di utilizzare nel DetailViewModel l'evento di completion per aggiornare la view solo una volta caricata effettivamente l'immagine. Con questa soluzione ho evitato di utilizzare la libreria SDWebImage e lasciare il count delle librerie esterne a 1, come da richiesta Bonus Task :-)

Ho utilizzato una Extension di UIWebImage che permette di aggiungere gli angoli arrotondati ad un'immagine e personalizzarne il raggio (opzionale, con impostazione di default a 20).

Ho creato una classe AppConfig che contiene tutte le costanti utilizzate nell'app, ed evita l'hardcoding di stringhe e valori, consentendo nello stesso tempo un'unico posto in cui questi valori posso essere eventualmente modificati.

Nel MenuViewController (lista Pokemon) ho implementato le seguenti funzionalità:
- gestione di mancanza di connessione con custom screen
- implementazione infinite scrolling e pagination per non caricare tutta la lista in un solo colpo (con conseguente ottimizzazione tempi e risorse)
- visualizzazione info basic del Pokemon con thumbnail, nome e id

Come da richiesta dei Bonus Task ho implementato le seguenti funzionalità:
- visualizzazione del numero degli elementi attualmente contenuti nella lista
- spacing delle celle tramite impostazione dei margini nel didLayoutViews
- possibilità di ordinamento della lista per ID o alfabeticamente
- possibilità di ricerca di un Pokemon specifico per nome
(questa funzionalità ha richiesto l'implementazione di una strategia ulteriore nella gestione dell'infinite scrolling)
- Inoltre ho aggiunto un Easter Egg: scuotendo il device viene caricato il dettaglio di un Pokemon a caso presente nella lista :-)

Nel DetailViewController ho implementato le seguenti funzionalità:
- Custom font per la visualizzazione del nome del Pokemon selezionato
- Visualizzazione dell'immagine ad alta risoluzione con background e angoli arrotondati
- Gestione di una differente dimensione dell'immagine per iPad tramite modifica dei LayoutConstraints.
- Gestione dell'attributedText per la TextView dei risultati (stats e type come da richiesta), evitando di utilizzare una libreria esterna tipo BonMot :-)

Inoltre ho notato che i dati di alcuni Pokemon non vengono decodificati correttamente, probabilmente per piccole difformità del formato JSON restituito. Questa evenienza viene gestita con un messaggio specifico e non blocca l'app.

Ho deciso di non visualizzare la navigationBar di sistema ma adottare un approccio fullscreen.

Nelle closures ho utilizzato [weak self] o [unowned self] dove necessario per evitare retain cycles.

Ho effettuato il safe unwrapping degli optionals tramite guard, if let e ??.

Ho utilizzato higher order functions (filter, map, reduce) sugli array per evitare cicli for-in e conformarmi ai paradigmi del functional programming.

Ho utilizzato AutoLayout per tutti gli elementi della UI.

Ho controllato tramite il tool Instruments la presenza di eventuali memory leaks, con esito negativo.

Vi ringrazio per l'attenzione, se qualcosa non vi è chiaro o se necessitate di ulteriori informazioni contattatemi pure ai recapiti forniti.

Cordialmente,

Paolo Piccini





