//
//  DetailViewModel.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import Foundation
import RxSwift
import RxCocoa

class DetailViewModel {
    
    var selectedPokemon: PokemonBasicData
    var pokemonDetailData = PublishSubject<PokemonDetail>()
    var error: Error?
    let disposeBag = DisposeBag()
    var networkLayer = NetworkLayer()
    var urlString: String
    
    init(selectedPokemon: PokemonBasicData) {
        self.selectedPokemon = selectedPokemon
        urlString = AppConfig.POKEMON_DETAIL_URL.replacingOccurrences(of: "%s", with: selectedPokemon.pokemonID)
    }
    
    public func fetchDetailData() {
        
        let disposable = networkLayer.fetchDataFromAPIv2(withUrlString: urlString, modelType: PokemonDetail.self)
            
            .subscribe(onNext: { data in
                
                guard let model = data else {
                    self.pokemonDetailData.onError(PokemonErrors.JSONParsingError)
                    return
                }

                self.pokemonDetailData.onNext(model)
                
            }, onError: { (error) in
                self.error = error
            }, onCompleted: {
                
            }) {
                print("Disposed")
            }
        disposable.disposed(by: disposeBag)
    }
    
    func getSelectedPokemonName() -> String {
        selectedPokemon.name
    }
    
    
}
