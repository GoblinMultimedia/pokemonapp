//
//  UIImageView+Extensions.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import Foundation
import UIKit
import RxSwift


let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    // adds rounded corners to an image
    // with an optional parameter to customize corner radius
    
    func makeRounded(radius: CGFloat = 20) {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
}
