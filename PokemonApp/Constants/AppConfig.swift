//
//  Constants.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import Foundation

// contains the basic constants for app configuration
// to avoid hardcoding of strings and values
// and makes them easy to modify as a single source of truth

enum PokemonErrors: Error {
    case JSONParsingError
}

class AppConfig {

    static let POKEMON_BASIC_DATA_INITIAL_URL = "https://pokeapi.co/api/v2/pokemon"
    static let POKEMON_DETAIL_URL = "https://pokeapi.co/api/v2/pokemon/%s/"
    static let THUMBNAIL_IMAGE_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/%s.png"
    static let ITEMS_PER_PAGE = 20
}


