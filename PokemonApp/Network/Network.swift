//
//  Network.swift
//  PokemonApp
//
//  Created by Paolo Piccini on 24/09/2020.
//

import Foundation
import RxSwift

// layer to manage network connections
// funcs use generics and follow the reactive paradigm
// returning observables

class NetworkLayer {
    
    public func fetchDataFromAPIv2<T: Codable>(withUrlString : String, modelType: T.Type) -> Observable<T?> {
        
        Observable<T?>.create { observer  in
            
            URLSession.shared.dataTask(with: URL(string: withUrlString)!) { (data, response, error) in
    
                let model = try? JSONDecoder().decode(T.self, from: data!)
                
                observer.onNext(model)
                
                if error != nil {
                    observer.onError(error!)
                }
                
                observer.onCompleted()
                
            }.resume()
            
            let disposable = Disposables.create()
            return disposable
            
        }
    }
    
    
    
    func imageFromRemote2(_ URLString: String, placeHolder: UIImage?) -> Observable<UIImage> {
        
        Observable<UIImage>.create { observer  in
            
            let imageServerUrl = URLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            if let cachedImage = imageCache.object(forKey: NSString(string: imageServerUrl)) {
                observer.onNext(cachedImage)
                
                let disposable = Disposables.create()
                return disposable
                
            }
            
            if let url = URL(string: imageServerUrl) {
                
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    
                    let httpResponse = response as? HTTPURLResponse
                    
                    if error != nil || httpResponse?.statusCode ?? 500 > 200 {
                        print("ERROR LOADING IMAGES FROM URL: \(url.absoluteString)")
                        DispatchQueue.main.async {

                            observer.onNext(placeHolder ?? UIImage())
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        if let data = data {
                            if let downloadedImage = UIImage(data: data) {
                                imageCache.setObject(downloadedImage, forKey: NSString(string: imageServerUrl))

                                observer.onNext(downloadedImage)
                            }
                        }
                    }
                }).resume()
            }
            
            let disposable = Disposables.create()
            return disposable
        }
        
    }
    
    
    
    
    
    
    
    
}
